// -----------------------------------------------------------------------
//	qiita.js
//
//					Nov/08/2024
//
// -----------------------------------------------------------------------
'use strict'

var array_aa = []
// -----------------------------------------------------------------------
window.onload = ()=>
{
	document.querySelector("#outarea_aa").innerHTML = "*** qiita.js *** start ***"

	const file_json = "./qiita_out.json"

	read_fetch_table_proc(file_json,".contents")

	document.querySelector("#outarea_hh").innerHTML = "*** qiita.js *** end ***"
}

// -----------------------------------------------------------------------
// [8]:
function filter_proc(obj)
{
	const id_select = obj.id

	document.querySelector("#outarea_bb").innerHTML = "obj.id = " + obj.id



//	var ids = document.getElementsByTagName('button')
	var elements = document.querySelectorAll('button')
	document.querySelector("#outarea_cc").innerHTML = elements.length

	for (var it=0; it<elements.length; it += 1)
		{
		document.getElementById(elements[it].id).style.color = "black"
		}

	document.getElementById(obj.id).style.color = "blue"

	var str_out = ""
	var data_new = []

	switch (id_select)
		{
	 	case "all":
			str_out = display_table_proc (array_aa)
			break

		case "likes":
			data_new = filter_likes_proc(array_aa)
			str_out = display_table_proc(data_new)
			break

		case "likes_sorted":
			data_new = filter_likes_proc(array_aa)
			data_new.sort(compare_proc)
			str_out = display_table_proc(data_new)
			break

		default:
			data_new = filter_items_proc (array_aa,id_select)
			str_out = display_table_proc(data_new)
			break
		}

	document.querySelector(".contents").innerHTML = str_out
}

// -----------------------------------------------------------------------
function filter_likes_proc (rec)
{
	var data_new = []
	rec.forEach(function(unit)
		{
		if (0 < unit.likes_count)
			{
			data_new.push(unit)
			}
		})

	var str_tmp = ""
	str_tmp += data_new.length + "<br />"

//	jQuery("#outarea_cc").html(str_tmp)

	return	data_new
}

// -----------------------------------------------------------------------
function filter_items_proc (rec,id_select)
{
	var tag_targets = [""]

	switch (id_select)
		{
		case 'python':
			tag_targets = ['Python3','Python']
			break

		case 'php':
			tag_targets = ['PHP','PHP5.6','PHP7','PHP7.4','PHP8']
			break

		case 'perl':
			tag_targets = ['Perl']
			break

		case 'nodejs':
			tag_targets = ['Node.js']
			break

		case 'TypeScript':
			tag_targets = ['TypeScript']
			break

		case 'Deno':
			tag_targets = ['Deno']
			break

		case 'jQuery':
			tag_targets = ['jQuery']
			break

		case 'FetchAPI':
			tag_targets = ['FetchAPI']
			break

		case 'golang':
			tag_targets = ['golang','Go']
			break

		case 'ruby':
			tag_targets = ['Ruby']
			break

		case 'Java':
			tag_targets = ['Java']
			break

		case 'cplus':
			tag_targets = ['C++']
			break

		case 'csharp':
			tag_targets = ['C#']
			break

		case 'fsharp':
			tag_targets = ['F#']
			break

		case 'R':
			tag_targets = ['R']
			break

		case 'Prolog':
			tag_targets = ['Prolog','SWI-Prolog']
			break

		case 'Bash':
			tag_targets = ['Bash']
			break

		case 'curl':
			tag_targets = ['curl']
			break

		case 'rust':
			tag_targets = ['Rust']
			break

		case 'Dart':
			tag_targets = ['Dart']
			break

		case 'Julia':
			tag_targets = ['Julia']
			break

		case 'Kotlin':
			tag_targets = ['Kotlin']
			break

		case 'Elixir':
			tag_targets = ['Elixir']
			break

		case 'Swift':
			tag_targets = ['Swift']
			break

		case 'AWS':
			tag_targets = ['AWS','aws-cli','APIGateway','AppSync']
			break

		case 'AWSIoT':
			tag_targets = ['awsIoT']
			break

		case 'Lambda':
			tag_targets = ['lambda']
			break

		case 'Bluemix':
			tag_targets = ['Bluemix']
			break

		case 'gcloud':
			tag_targets = ['gcloud','GoogleCloudPlatform']
			break

		case 'GoogleCloudFunctions':
			tag_targets = ['GoogleCloudFunctions']
			break

		case 'Azure':
			tag_targets = ['Azure','AzureIoTHub','MicrosoftAzure']
			break

		case 'Heroku':
			tag_targets = ['Heroku']
			break

		case 'Domo':
			tag_targets = ['Domo']
			break

		case 'Facebook':
			tag_targets = ['Facebook']
			break

		case 'Twitter':
			tag_targets = ['Twitter','TwitterAPI']
			break

		case 'chatwork':
			tag_targets = ['Chatwork']
			break

		case 'GitHub':
			tag_targets = ['GitHub']
			break

		case 'GitLab':
			tag_targets = ['GitLab']
			break

		case 'COTOHA':
			tag_targets = ['COTOHA']
			break

		case 'Kaggle':
			tag_targets = ['Kaggle']
			break

		case 'SORACOM':
			tag_targets = ['SORACOM','SoracomHarvest']
			break

		case 'Yahoo':
			tag_targets = ['YahooAPI']
			break

		case 'Conoha':
			tag_targets = ['Conoha','ConoHaObjectStorage']
			break

		case '生成AI':
			tag_targets = ['ChatGPT','GEMINI','llama2','wrtn','Command-R+','groq']
			break

		case 'DynamoDB':
			tag_targets = ['DynamoDB']
			break

		case 'FireStore':
			tag_targets = ['Firestore']
			break

		case 'MongoDB':
			tag_targets = ['MongoDB']
			break

		case 'CouchDB':
			tag_targets = ['CouchDB']
			break

		case 'Redis':
			tag_targets = ['Redis']
			break

		case 'MariaDB':
			tag_targets = ['mariadb','MySQL']
			break

		case 'PostgreSQL':
			tag_targets = ['PostgreSQL']
			break

		case 'Oracle':
			tag_targets = ['OracleDatabase']
			break

		case 'SqlServer':
			tag_targets = ['SQLServer']
			break

		case 'Sqlite':
			tag_targets = ['sqlite','SQLite3']
			break

		case 'Elasticsearch':
			tag_targets = ['Elasticsearch']
			break

		case 'InfluxDB':
			tag_targets = ['influxdb']
			break

		case 'Excel':
			tag_targets = ['Excel','XLSX']
			break

		case 'GoogleSpreadSheet':
			tag_targets = ['GoogleSpreadSheet']
			break

		case 'PDF':
			tag_targets = ['PDF','mpdf']
			break

		case 'Django':
			tag_targets = ['Django']
			break

		case 'Flask':
			tag_targets = ['Flask']
			break

		case 'Bottle':
			tag_targets = ['bottle']
			break

		case 'FastAPI':
			tag_targets = ['FastAPI']
			break

		case 'RoR':
			tag_targets = ['RubyonRails7.0','RubyonRails6.1']
			break

		case 'Laravel':
			tag_targets = ['laravel5.6','laravel10','Laravel']
			break

		case 'FuelPHP':
			tag_targets = ['fuelphp1.8']
			break

		case 'Flight':
			tag_targets = ['Flight']
			break

		case 'Genie':
			tag_targets = ['Genie']
			break

		case 'Express':
			tag_targets = ['Express']
			break

		case 'Koa.js':
			tag_targets = ['Koa.js']
			break

		case 'Nest.js':
			tag_targets = ['NestJS']
			break

		case 'Node-RED':
			tag_targets = ['node-red']
			break

		case 'Echo':
			tag_targets = ['echo']
			break

		case 'Gin':
			tag_targets = ['gin']
			break

		case 'Vue.js':
			tag_targets = ['Vue.js','nuxt.js']
			break

		case 'React.js':
			tag_targets = ['React']
			break

		case 'Next.js':
			tag_targets = ['next.js']
			break

		case 'Solid.js':
			tag_targets = ['SolidJS']
			break

		case 'WordPress':
			tag_targets = ['WordPress']
			break

		case 'EC-CUBE':
			tag_targets = ['EC-CUBE4','ECCUBE2']
			break

		case 'Grafana':
			tag_targets = ['grafana']
			break

		case 'dotnet':
			tag_targets = ['.NET']
			break

		case 'Scraping':
			tag_targets = ['scraping','beautifulsoup4']
			break

		case 'Web':
			tag_targets = ['Web']
			break

		case 'Nginx':
			tag_targets = ['nginx']
			break

		case 'Apache':
			tag_targets = ['Apache','Apache2.4']
			break

		case 'Tomcat':
			tag_targets = ['Tomcat','tomcat9']
			break

		case 'uWSGI':
			tag_targets = ['uwsgi']
			break

		case 'Selenium':
			tag_targets = ['Selenium','SeleniumGrid']
			break

		case 'HTTP':
			tag_targets = ['HTTP','HttpClient']
			break

		case 'WebDAV':
			tag_targets = ['webdav']
			break

		case 'udp':
			tag_targets = ['udp']
			break

		case 'mqtt':
			tag_targets = ['mqtt','mqtts','paho-mqtt','mosquitto','paho','AzureIoTHub']
			break

		case 'Rest-API':
			tag_targets = ['REST-API']
			break

		case 'gRPC':
			tag_targets = ['gRPC']
			break

		case 'mail':
			tag_targets = ['mail','imap','postfix','Gmail']
			break

		case 'dotenv':
			tag_targets = ['dotenv']
			break

		case 'OCR':
			tag_targets = ['OCR']
			break

		case 'WebAPI':
			tag_targets = ['WebAPI']
			break

		case 'LetsEncrypt':
			tag_targets = ['letsencrypt','Let’sEncrypt']
			break

		case 'JWT':
			tag_targets = ['JWT']
			break

		case 'deb':
			tag_targets = ['deb']
			break

		case 'upload':
			tag_targets = ['upload']
			break

		case 'cookie':
			tag_targets = ['cookie']
			break

		case 'base64':
			tag_targets = ['base64']
			break

		case 'GraphQL':
			tag_targets = ['GraphQL']
			break

		case 'serial':
			tag_targets = ['serial']
			break

		case 'Operation':
			tag_targets = ['運用']
			break

		case 'Tutorial':
			tag_targets = ['tutorial']
			break

		case 'SoftEther':
			tag_targets = ['SoftEther_VPN','vpncmd']
			break

		case 'covid19':
			tag_targets = ['コロナウイルス']
			break

		case 'machine_learning':
			tag_targets = ['機械学習']
			break

		case 'audio':
			tag_targets = ['audio']
			break

		case 'TextToSpeech':
			tag_targets = ['TextToSpeech']
			break

		case '仮想環境':
			tag_targets = ['仮想環境','lxc','lxd','Docker','Incus']
			break

		case '翻訳':
			tag_targets = ['翻訳']
			break
			
		case '自然言語処理':
			tag_targets = ['自然言語処理','mecab','GiNZA']
			break

		case 'Graphics':
			tag_targets = ['graph','d5.js','chart.js','canvas',
				'grafana','Kibana']
			break
			
		case '動画配信':
			tag_targets = ['動画配信']
			break
			
		case '音声認識':
			tag_targets = ['音声認識']
			break
			
		case '画像処理':
			tag_targets = ['画像処理','ImageMagick']
			break
			
		case 'pandas':
			tag_targets = ['pandas']
			break

		case 'pillow':
			tag_targets = ['pillow']
			break

		case 'beautifulsoup':
			tag_targets = ['beautifulsoup4']
			break

		case 'SQLAlchemy':
			tag_targets = ['sqlalchemy']
			break

		case 'ArchLinux':
			tag_targets = ['archLinux']
			break

		case 'Ubuntu':
			tag_targets = ['Ubuntu']
			break

		case 'CentOS':
			tag_targets = ['centos7','CentOS']
			break

		case 'Alpine':
			tag_targets = ['alpine']
			break

		case 'Raspberry':
			tag_targets = ['Raspberry','RaspberryPi']
			break

		case 'Arduino':
			tag_targets = ['Arduino','ArduinoUno','ArduinoUnoR4WiFi','ArduinoIDE']
			break

		case 'ESP32':
			tag_targets = ['ESP32']
			break

		case 'WioLTE':
			tag_targets = ['WioLTE']
			break

		case 'M5StackCore2':
			tag_targets = ['M5stack','M5stackCore2']
			break

		case 'M5Paper':
			tag_targets = ['M5Paper']
			break

		case 'ChromeBook':
			tag_targets = ['Chromebook']
			break

		case 'Android':
			tag_targets = ['Android','AndroidStudio']
			break

		}

	var data_new = []

//	rec.forEach(function(unit)
	array_aa.forEach(function(unit)
		{
		var flag_out = false
		unit.tags.forEach(function(tag)
			{
			for (var it in tag_targets)
				{
				if (tag.name == tag_targets[it])
					{
					flag_out = true
					}
				}
			})

		if (flag_out)
			{
			data_new.push(unit)
			}
		})

	return	data_new
}

// -----------------------------------------------------------------------
function compare_proc(left,right)
{
	const aa = left.likes_count
	const bb = right.likes_count

	var rvalue = 0


	if (bb < aa)
		{
		rvalue = -1
		}
	else if (aa < bb)
		{
		rvalue = 1
		}
	else
		{
		const pp = left.no
		const qq = right.no

		if (qq < pp)
			{
			rvalue = -1
			}
		else if (pp < qq)
			{
			rvalue = 1
			}
		}

	return	rvalue
}

// -----------------------------------------------------------------------
